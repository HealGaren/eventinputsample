public abstract class Input<TInfo, TParam> {

	public interface OnInputListener<T> {
		void onInput(T param);
	}

	public final TInfo info;
	private OnInputListener<TParam> onInputListener;

	public Input(TInfo info, OnInputListener<TParam> onInputListener) {
		this.info = info;
		this.onInputListener = onInputListener;
	}

	public void resolve(TParam param) {
		this.onInputListener.onInput(param);
	}
}
