import java.util.LinkedList;

public class Game {

	private LinkedList<Input<?, ?>> queue = new LinkedList<>();

	public <TInfo, TParam> void queueInput(Input<TInfo, TParam> input) {
		queue.add(input);
	}

	public <TInfo, TParam> Input<TInfo, TParam> popInput() {
		return (Input<TInfo, TParam>) queue.pop();
	}

}
