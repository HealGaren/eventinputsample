import java.util.List;
import java.util.Scanner;

public class Main {

	private static Game game = new Game();

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		// 이벤트 내에서 input 요청
		queueRPSInput();

		while (true) {
			Input<?, ?> _currentInput = game.popInput();
			if (_currentInput instanceof RPSInput) {
				RPSInput currentInput = (RPSInput) _currentInput;
				System.out.println("가위, 바위, 보 중 선택하세요:");
				String input = sc.nextLine();
				currentInput.resolve(RPSInput.Type.fromKorean(input));
			} else if (_currentInput instanceof SelectCardInput) {
				SelectCardInput currentInput = (SelectCardInput) _currentInput;
				System.out.println(currentInput.info.name);
				System.out.println(String.join(", ", currentInput.info.selectableCardIds) + " 중 하나를 선택하세요");
				int index = sc.nextInt();
				sc.nextLine();
				currentInput.resolve(currentInput.info.selectableCardIds.get(index));
			}
		}
	}

	private static void queueRPSInput() {
		game.queueInput(new RPSInput(rpsType -> {
			System.out.println("당신이 낸 것 : " + rpsType.name());
			queueSelectCardInput();
		}));
	}

	private static void queueSelectCardInput() {
		game.queueInput(new SelectCardInput(
				new SelectCardInput.Info(
					"발견: 3장 중 하나를 선택하세요",
					List.of("전시병의 외침", "개발자의 최후", "라그투스의 가호")
				),
				param -> {
					System.out.println("당신의 손으로 " + param + "을(를) 가져옵니다.");
					queueRPSInput();
				}
		));
	}
}
