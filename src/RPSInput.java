public class RPSInput extends Input<Void, RPSInput.Type> {

	public enum Type {
		ROCK,
		PAPER,
		SCISSORS;

		public static Type fromKorean(String string) {
			switch (string) {
				case "가위":
					return SCISSORS;
				case "바위":
					return ROCK;
				case "보":
					return PAPER;
			}
			return null;
		}
	}

	public RPSInput(OnInputListener<Type> onInputListener) {
		super(null, onInputListener);
	}
}
