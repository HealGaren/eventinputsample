import java.util.List;

public class SelectCardInput extends Input<SelectCardInput.Info, String> {

	public static class Info {
		public String name;
		public List<String> selectableCardIds;

		public Info(String name, List<String> selectableCardIds) {
			this.name = name;
			this.selectableCardIds = selectableCardIds;
		}
	}

	public SelectCardInput(Info info, OnInputListener<String> onInputListener) {
		super(info, onInputListener);
	}

}
